export default {
  usernameInput: 'input[id="uid"]',
  passwordInput: 'input[id="passw"]',
  loginBtn: 'input[name="btnSubmit"]'
}
